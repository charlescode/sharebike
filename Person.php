<?php
namespace ShareBike;

class Person 
{
	
	protected $name;
	protected $mobile;
	protected $rideHistory = [];

	function __construct(string $name, Mobile $mobile = null) 
	{
		$this->name = $name;
		$this->mobile = $mobile;

	}

	public function getName() 
	{
		return $this->name;
	}

	public function setMobile(Mobile $mobile) 
	{
		$this->mobile = $mobile;
		return $this;
	}

	public function getMobile() 
	{
		return $this->mobile;
	}

	/**
	 * when people ride bike, then recode ride the destination, distance, speed, spend time
	 */ 
	public function ride(Bike $bike, Place $place) : array
	{
		$arr = [];
		$distance = 0;
		$averageSpeed = 0;
		$timeTotal = 0;
		$routes = $place->getRoute();


		foreach ($routes as $key => $value) {
			if ($value[3] == 0) {
					$bike->command('lock');
					return $arr;
			}
		}
		if (!$this->mobile->scanBarcode($bike)) {
			echo "no enough money to ride Share Bike, please charge first\n";
			return $arr;
		}

		//ride history key.
		$rideKey = $bike->getBarcode() . str_pad(random_int(0, 999), 3, '0', STR_PAD_LEFT);
		// echo "key is $rideKey<br>";
		//$routes value[0] is passed place name, value[1] is the place's coordinate, value[2] is the distance, 
		//value[3] is the speed.
		foreach ($routes as $key => $value) {
				$distance += $value[2];
				$timeTotal += round($value[2]/ $value[3],2); 
				$bike->response('coordinate', ['coordinate' => $value[1], 'placeName' => $value[0]]);
				$this->rideHistory[$rideKey][$value[0]] = $value[1];
		}
		if ($timeTotal == 0) {
			$averageSpeed = 0;
		} else {
			$averageSpeed = round($distance/$timeTotal, 2);
		}

		$arr['distance'] = $distance;
		$arr['timeSpend'] = $timeTotal;
		$arr['speed'] = $averageSpeed;
		$arr['placeName'] = $place->getName();
		//lock bike.
		$bike->command('lock'); 

		return $arr;
	}

	public function getRideHistory() 
	{
		return $this->rideHistory;
	}



} 