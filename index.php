<?php
use ShareBike\Bike;
use ShareBike\Mobile;
use ShareBike\Cloud;
use ShareBike\Person;
use ShareBike\Place;
use ShareBike\Factory;

require_once('/var/www/html/practise/hospital/sharebike/Cloud.php');
require_once('/var/www/html/practise/hospital/sharebike/Bike.php');
require_once('/var/www/html/practise/hospital/sharebike/Mobile.php');
require_once('/var/www/html/practise/hospital/sharebike/Person.php');
require_once('/var/www/html/practise/hospital/sharebike/Place.php');
require_once('/var/www/html/practise/hospital/sharebike/Factory.php');
?>

<!DOCTYPE html>
<html>
<head>
	<title>Share Bike</title>
	<script type="text/javascript">
		
	</script>
	<link href = 'style.css' rel = 'stylesheet' type ='text/css' /> 
</head>
<body>
	<div class = 'main'>
<?php
$factory = new Factory();
$bike1 = $factory->make('bike','bike1', ['x' =>'141', 'y' => '220']);
echo "<div id ='".htmlspecialchars($bike1->getBarcode())."'>";
echo htmlspecialchars($bike1->getBarcode());
echo "</div>";

$bike2 = $factory->make('bike','bike2', ['x' =>'41', 'y' => '20']);
echo "<div id ='".htmlspecialchars($bike2->getBarcode())."'>";
echo htmlspecialchars($bike2->getBarcode());
echo "</div>";

$bike3 = $factory->make('bike','bike3', ['x' =>'81', 'y' => '60']);
echo "<div id ='".htmlspecialchars($bike3->getBarcode())."'>";
echo htmlspecialchars($bike3->getBarcode());
echo "</div>";

$bike4 = $factory->make('bike','bike4', ['x' =>'501', 'y' => '400']);
echo "<div id ='".htmlspecialchars($bike4->getBarcode())."'>";
echo htmlspecialchars($bike4->getBarcode());
echo "</div>";

$bike5 = $factory->make('bike','bike5', ['x' =>'270', 'y' => '300']);
echo "<div id ='".htmlspecialchars($bike5->getBarcode())."'>";
echo htmlspecialchars($bike5->getBarcode());
echo "</div>";

$honor = $factory->make('mobile', 'Honor', ['x' => '20', 'y' => '100']);
echo "<div id ='mobile'>";
echo htmlspecialchars($honor->getName());
echo "</div>";
$arr = $honor->findBike([$bike1, $bike2, $bike3, $bike4, $bike5]);
//var_dump($arr);

//init person obj
$person = new Person('Charles', $honor);


$place = new Place('SummerPalace');
$place->addRoute('Zhongguancun', ['x' => '160','y' => '151'], 30, 20);
$place->addRoute('Xierqi', ['x' => '260','y' =>'221'], 20, 25);
$place->addRoute('YuanmingPark', ['x' => '360','y' =>'327'], 23, 28);
$place->addRoute('SummerPalace', ['x' => '460','y' =>'443'], 15, 23);

$honor->charge(20);

$arr = $person->ride($bike3, $place);
// var_dump($arr);
$routes = $place->getRoute();
foreach ($routes as $key => $route) {
	echo "<div id = '".htmlspecialchars(strtolower($route[0]))."'>";
	echo htmlspecialchars($route[0]);
	echo "</div>";
}
$fee = $honor->request('fee', $arr['timeSpend'] * 60);
$honor->pay($fee);
echo "First time balance is ".htmlspecialchars($honor->getBalance())."; ";

$honor->charge(20);
$placeAnother = new Place('Shougang Iron Factory');
$placeAnother->addRoute('Gongzhufen', ['x' => '207','y' => '210'], 40, 20);
$placeAnother->addRoute('Wukesong', ['x' => '406','y' =>'198'], 30, 25);
$placeAnother->addRoute('Babaoshan', ['x' => '600','y' =>'180'], 25, 27);
$placeAnother->addRoute('ShougangIronFactory', ['x' => '707','y' =>'230'], 22, 17);

$arrNew = $person->ride($bike3, $placeAnother);

$routes = $placeAnother->getRoute();
foreach ($routes as $key => $route) {
	echo "<div id = '".htmlspecialchars(strtolower($route[0]))."'>";
	echo htmlspecialchars($route[0]);
	echo "</div>";
}
$fee = $honor->request('fee', $arrNew['timeSpend'] * 60);
$honor->pay($fee);
echo "Second time balance is ".htmlspecialchars($honor->getBalance())."; ";


$honor->charge(30);

$placeAnother = new Place('Yizhuang');
$placeAnother->addRoute('Qianmen', ['x' => '460','y' => '70'], 10, 20);
$placeAnother->addRoute('Caishikou', ['x' => '440','y' =>'150'], 10, 25);
$placeAnother->addRoute('TiantanPark', ['x' => '480','y' =>'257'], 25, 27);
$placeAnother->addRoute('Yizhuang', ['x' => '390','y' =>'530'], 30, 17);

$arrAnother = $person->ride($bike3, $placeAnother);
$fee = $honor->request('fee', $arrAnother['timeSpend'] * 60);

$routes = $placeAnother->getRoute();
foreach ($routes as $key => $route) {
	echo "<div id = '".htmlspecialchars(strtolower($route[0]))."'>";
	echo htmlspecialchars($route[0]);
	echo "</div>";
}
$honor->pay($fee);
echo "Third time balance is ".htmlspecialchars($honor->getBalance())."<br>";
// var_dump($person->getRideHistory());
// var_dump($bike3->getRecordPosition());


?>


	</div>

</body>
</html>
