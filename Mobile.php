<?php
namespace ShareBike;

class Mobile implements Cloud 
{
	const MIN_CREDIT = 20;
	protected $currentBalance;
	protected $coordinate;

	function __construct(string $name, array $coordinate) 
	{
		$this->name = $name;
		$this->coordinate = $coordinate;
	}

	public function getName()
	{
		return $this->name;
	}

	/**
	 * according the mobile location to find the nearest bike position.
	 */
	public function findBike(array $bikes = []) : array
	{
		$arr = [];
		$shortDistance = 99999;
		$x = $this->coordinate['x'];
		$y = $this->coordinate['y'];

		foreach ($bikes as $key => $bike) {
			$x1 = $bike->getCoordinate()['x'];
			$y1 = $bike->getCoordinate()['y'];
			$tempDistance =  round(sqrt (($x - $x1) * ($x - $x1) + ($y - $y1) * ($y - $y1)),2);
			if ($tempDistance < $shortDistance) {
				$shortDistance = $tempDistance;
				$temp = $key;
			}

		}
		$arr['distance'] = $shortDistance;
		$arr['bike'] = $bikes[$temp];
		return $arr;
	}

	/**
	 * the bike has it's unique barcode
	 */
	public function scanBarcode(Bike $bike) : bool
	{
		$barcode = $bike->getBarcode();
		//unlock successful
		if ($this->request('unlock')) {
			$bike->command('unlock');
			$say = 'success';
			$result = true;
		} else {
			$say = 'failed';
			$result = false;
		}
		return $result;
	}

	
	public function getBalance() 
	{
		return $this->currentBalance;
	}

	public function charge(float $money) 
	{
		$this->currentBalance += $money; 
	}


	public function pay(float $money) : bool
	{
		if ($this->currentBalance >= $money) {

			$this->currentBalance -= $money;
		} else {
			echo "You do not have enough money to pay\n";
			return false;
		}
		return true;
	}

	/**
	 * check if you have enough credit to use share bike.
	 */
	public function checkCredit() : bool 
	{

		if ($this->currentBalance >= self::MIN_CREDIT) {
			return true;
		}
		return false;
	}

	/**
	 * mobile make request to cloud
	 */ 
	public function request(string $type, int $periodMinutes = 0) 
	{
		if ($type == 'unlock'){
			if ($this->checkCredit()) {
				return true;
			}
			return false;
		}	
		if ($type == 'lock') {
			return true;
		}
		if ($type == 'fee') {
			return $this->calculate($periodMinutes);
		}
			
		return false;
	}

	public function response(string $type, array $value) 
	{

	}

	public function command(string $type) 
	{

	}

	/**
	 * 	this function to calculate how much money you need to pay depend on the used minutes.
	 */
	public function calculate(int $periodMinutes) : float
	{
		$pay = 0;
		if ($periodMinutes == 0) {
			$pay = 0;
		}
		if ($periodMinutes <= 15) {
			$pay = 5;
		} else if ($periodMinutes <= 60) {
		 	$pay = 6.6;
		} else if ($periodMinutes <= 120) {
			$pay = 9.7;
		} else if ($periodMinutes <= 180) {
			$pay = 13;
		} else if ($periodMinutes <= 240) {
			$pay = 15.2;
		} else if ($periodMinutes <= 300) {
			$pay = 17.4;
		} else {
			$pay = 19.5;
		}
		return $pay;
	}

}