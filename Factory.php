<?php
namespace ShareBike;

class Factory 
{
	public function make(string $type, string $name, array $coordinate) 
	{
		switch($type) {
			case 'bike':
				$obj = new Bike($name, $coordinate);
				break;
			case 'mobile':
				$obj = new Mobile($name, $coordinate);
				break;	

		}
		return $obj;

	}

}
