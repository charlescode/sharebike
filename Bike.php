<?php
namespace ShareBike;

class Bike implements Cloud 
{

	protected $barcode;
	protected $coordinate =[];
	protected $locker = true;
	protected $recordPositon = [];


	function __construct($barcode , $coordinate = ['x' =>0,'y' =>0]) 
	{
		$this->barcode = $barcode;
		$this->coordinate = $coordinate;
		//record bike original location, the bike will move to some places.
		$this->recordPositon['Home'] = $coordinate;
	}


	public function getBarcode() 
	{
		return $this->barcode;
	}

	public function getLockStatus() 
	{
		return $this->locker;

	}

	private function setLockStatus(bool $status = false) 
	{
		$this->locker = $status;
	}

	public function getRecordPosition() 
	{
		return $this->recordPositon;

	}

	private function setCoordinate(array $coordinate , string $placeName = 'home') 
	{
		$this->coordinate = $coordinate;
		$this->placeName = $placeName;
		$this->recordPositon[$placeName] = $coordinate;
		return $this;  
	}

	public function getCoordinate() 
	{
		return $this->coordinate;
	}

	/**
	 * interface function
	 */
	public function request(string $type, int $value) 
	{

	}
	
	/**
	 * interface function, the bike response it's location and place.
	 */
	public function response(string $type, array $value) 
	{	
		if ($type == 'coordinate') {
			$this->setCoordinate($value['coordinate'], $value['placeName']);
		}
		return;
	}

	/**
	 * interface function, lock or unlock the bike.
	 */
	public function command(string $type)
	{
		if ($type == 'unlock') {
			$this->setLockStatus(false);
		}
		if ($type == 'lock') {
			$this->setLockStatus(true);
		} 
		return;
	}

	
}
