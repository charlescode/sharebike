<?php
namespace ShareBike;

/**
 * The core software runs in the server, we call the server is cloud,
 * server controls all the bikes, get bike location and lock or unlock the bike
 * server gets request from mobile and response the request  
 */	
Interface Cloud 
{
	const ACTIONS = [
		'unlock' => 'unlock bike',
		'lock' => 'lock bike',
		'location' => 'tell me your location',
		'balance' => 'check your current balance',
		'pay' => 'pay money',
		'charge' => 'charge credit money',
		];

	public function response(string $type, array $value);
	public function request(string $type, int $value);
	public function command(string $type); 
}
