<?php 
namespace ShareBike;

/**
 * When ride bike to the destination place, we define place is class, before 
 * ride to the place, need pass some of other places, these places have name,
 * coordinate property, we define them in an array called route. 
 */ 
class Place 
{
	protected $route = [];
	protected $name;

	function __construct(string $name) 
	{
		$this->name = $name;
	}

	public function getName() 
	{
		return $this->name;
	}

	public function addRoute(string $passPlace, array $coordinate, float $distance, float $speed = 20) 
	{
		$this->route[] = [$passPlace, $coordinate, $distance, $speed];
		return $this;
	}

	public function getRoute() 
	{
		return $this->route;
	}
}