<?php 
use PHPUnit\Framework\TestCase;
use ShareBike\Bike;
use ShareBike\Mobile;
use ShareBike\Cloud;
use ShareBike\Person;
use ShareBike\Place;
use ShareBike\Factory;

require_once('/var/www/html/practise/hospital/sharebike/Cloud.php');
require_once('/var/www/html/practise/hospital/sharebike/Bike.php');
require_once('/var/www/html/practise/hospital/sharebike/Mobile.php');
require_once('/var/www/html/practise/hospital/sharebike/Person.php');
require_once('/var/www/html/practise/hospital/sharebike/Place.php');
require_once('/var/www/html/practise/hospital/sharebike/Factory.php');




class ShareBiekTest extends TestCase 
{

	public function setUp() 
	{

	}

	public function testMobile() 
	{
		$coordinate = ['x' => '0', 'y' => '0'];
		$mobile = new Mobile('Huawei', $coordinate);
		$mobile->charge(20);
		$this->assertEquals($mobile->getBalance(), 20);

		$mobile->pay(10);
		$this->assertEquals($mobile->getBalance(), 10);

		$this->assertFalse($mobile->checkCredit());

		$mobile->charge(30);
		$this->assertTrue($mobile->checkCredit());
		$this->assertEquals($mobile->getBalance(), 40);

		$mobile->pay(30);
		$this->assertEquals($mobile->getBalance(), 10);

		$this->assertFalse($mobile->pay(30));
		$this->assertEquals($mobile->getBalance(), 10);

		$this->assertEquals($mobile->request('fee', '120'), '9.7');
		$this->assertEquals($mobile->request('fee', '500'), '19.5');
		$this->assertEquals($mobile->request('fee', '300'), '17.4');


		$mobile->charge(50);


		$coordinateArr = [
			['x' => '60', 'y' => '40'],
			['x' => '20', 'y' => '20'],
			['x' => '30', 'y' => '30'],
			['x' => '40', 'y' => '40'],
			['x' => '50', 'y' => '51'],
		];



		$bikea= new Bike('a', $coordinateArr[0]);
		$bikeb= new Bike('b', $coordinateArr[1]);
		$bikec = new Bike('c', $coordinateArr[2]);
		$biked = new Bike('d', $coordinateArr[3]);
		$bikee = new Bike('e', $coordinateArr[4]);

		$bikes = [$bikea, $bikeb, $bikec, $biked, $bikee];

		$this->assertEquals('50', $bikee->getCoordinate()['x']);
		$this->assertEquals('51', $bikee->getCoordinate()['y']);
		$arr = $mobile->findBike($bikes);
		var_dump($arr);
		$this->assertEquals(28.28, $arr['distance']);
		$this->assertInstanceOf(Bike::class, $arr['bike']);

		$this->assertTrue($mobile->scanBarcode($bikea));

		$this->assertFalse($bikea->getLockStatus());


	}

	public function testRide() 
	{
		$coordinate = ['x' => '0', 'y' => '0'];
		$mobile = new Mobile('Zend', $coordinate);
		$mobile->charge(20);

		$person = new Person('Charles', $mobile);

		$place = new Place('Summer Palace');
		$place->addRoute('Zhongguancun', ['x' => '100','y' => '200'], 30, 20);
		$place->addRoute('Xierqi', ['x' => '200','y' =>'400'], 20, 25);
		$place->addRoute('Yuanming Park', ['x' => '400','y' =>'700'], 23, 28);
		$place->addRoute('Summer Palace', ['x' => '740','y' =>'909'], 15, 23);

		$bike = new Bike('Giant', ['x' =>'10', 'y'=>'20']);	

		$arr = $person->ride($bike, $place);
		$this->assertEquals('88', $arr['distance']);
		$this->assertEquals('3.77', $arr['timeSpend']);
		$this->assertEquals('23.34' , $arr['speed']);
		$this->assertEquals('Summer Palace' , $arr['placeName']);

		$rideHistory = $person->getRideHistory();
		$fee = $mobile->request('fee', $arr['timeSpend'] * 60);
		$mobile->pay($fee);
		//first ride balance 4.8
		$this->assertEquals($mobile->getBalance(), '4.8');
		
		$mobile->charge(20);

		//test another bike
		$placeAnother = new Place('Shougang Iron Factory');
		$placeAnother->addRoute('Gongzufen', ['x' => '100','y' => '2000'], 40, 20);
		$placeAnother->addRoute('Wukesong', ['x' => '200','y' =>'4000'], 30, 25);
		$placeAnother->addRoute('Babaoshan', ['x' => '400','y' =>'7000'], 25, 27);
		$placeAnother->addRoute('Shougang Iron Factory', ['x' => '607','y' =>'9090'], 22, 17);

		$bikeAnother = new Bike('Sportman', ['x' => '100', 'y'=> '1500']);
		$arrAnother = $person->ride($bikeAnother, $placeAnother);
		//var_dump($arrAnother);
		$this->assertEquals($arrAnother['speed'], '21.59');
		$this->assertEquals($arrAnother['placeName'], 'Shougang Iron Factory');
		$rideHistory = $person->getRideHistory();
		var_dump($rideHistory);
		// $this->assertArrayHasKey('Shougang Iron Factory', $rideHistory);
		// $this->assertArrayHasKey('Summer Palace', $rideHistory);

		$this->assertCount(2, $rideHistory);
		$fee = $mobile->request('fee', $arrAnother['timeSpend'] * 60);
		$mobile->pay($fee);
		//second ride balance is 5.3
		$this->assertEquals($fee, 19.5);

		$mobile->charge(20);


		// $this->assertArraySubset(['x'], [['x'],'y','z']);
		//test back the same bike before
		//test another bike
		$placeAnother = new Place('Yizhuang');
		$placeAnother->addRoute('Qianmen', ['x' => '1000','y' => '2000'], 10, 20);
		$placeAnother->addRoute('Chaishikou', ['x' => '2000','y' =>'2004'], 10, 25);
		$placeAnother->addRoute('Tiantan Park', ['x' => '4000','y' =>'2008'], 25, 27);
		$placeAnother->addRoute('Yizhuang', ['x' => '16070','y' =>'2018'], 30, 17);
		$arrNew = $person->ride($bike, $placeAnother);
		$this->assertEquals('20.89', $arrNew['speed']);
		// var_dump($bike->getRecordPosition());
		$rideHistory = $person->getRideHistory();
		var_dump($rideHistory);
		// $this->assertArrayHasKey('Yizhuang', $rideHistory);
		// $this->assertArrayHasKey('Shougang Iron Factory', $rideHistory);
		// $this->assertArrayHasKey('Summer Palace', $rideHistory);
		$this->assertCount(3, $rideHistory);
		$fee = $mobile->request('fee', $arrNew['timeSpend'] * 60);
		$mobile->pay($fee);
		$this->assertEquals($fee, 15.2);
		//third time ride bike balance is 10.1
		$this->assertEquals($mobile->getBalance(), 10.1);
	}

	public function testFactory() 
	{
		$factory = new Factory();
		$bike = $factory->make('bike','Giant', ['x' =>'5', 'y' => '10']);
		$this->assertInstanceOf(Bike::class, $bike);

		$honor = $factory->make('mobile','Honor', ['x' =>'15', 'y' => '100']);
		$this->assertInstanceOf(Mobile::class, $honor);
		$this->assertEquals('Honor' , $honor->getName());


	}

	public function tearDown() 
	{


	}

}

